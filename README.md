# Nyoba Tailwind

HTML + Tailwind CSS (Via CDN)

Jika sebelumnya presentasi saya [**Tailwind CSS in A Nutshell**](https://febriansyah-speak-tailwind.now.sh/) di SurabayaJS hanya memberikan contoh untuk membuat tombol & implementasi di GatsbyJS, maka ini contoh implementasi di file HTML biasa yang lebih detail

untuk cara ini (memakai Tailwind via CDN) sangat tidak saya sarankan utk production karena ukuran Tailwind nya sekitar 2MB dan belum tentu class nya dipakai semua 😂

## [Demo](https://febriansyahdotid.gitlab.io/nyoba-tailwind/)

## Screenshot
![image.png](./image.png)
